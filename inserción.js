const registroEsutdiante = async (req, res, next) => {
    const expresiones = {
		nombre: /^([a-zA-Z]{4,})\s([a-zA-Z]+)*$/, 
        cedula: /^\d{10}$/,
        letras: /^[A-Za-z]{4,35}/
	}
    const {
        nombre,
        apellido,
        cedula,
        semestre,
        paralelo
    } = req.body

    if(!expresiones.nombre.test(nombre)){
        res.json({'error': 'Nombre mal ingresado'})
    }else  if(!expresiones.nombre.test(apellido)){
        res.json({'error': 'Apellido mal ingresado'})
    }else if(!expresiones.cedula.test(cedula)){
        res.json({'error': 'Cedula mal ingresada'})
    }else if(!expresiones.letras.test(semestre)){
        res.json({'error': 'Semestre solo pueden ser letras'})
    }else if(!expresiones.letras.test(paralelo)){
        res.json({'error': 'Paralelo solo pueden ser letras'})
    }else{
            try {
        
                const newEst = await pool.query(
                    "INSERT INTO estudiante(nombre_est, apellido_est, ced_est, paralelo_est, semestre_est) VALUES($1, $2, $3, $4, $5) RETURNING *",
                    [nombre, apellido, cedula, paralelo, semestre]
                );
                if(newEst.rowCount == 1){
                    res.json(newEst.rows);
                }
            } catch (error) {
                res.json({'msgErr': error})
            }
    }
}


